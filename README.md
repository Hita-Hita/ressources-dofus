# Ressources Dofus

Ici seront rassemblées plusieurs ressources qui détaillent certains éléments de gameplay du MMO Dofus.

Ceci a uniquement pour but de clarifier des mécaniques peu ou mal expliquées nativement par le jeu.

## Sommaire

- [Entraves et Déplacements](./entraves_deplacements/README.md)
  - Tout ce que vous avez toujours voulu savoir sur les états Lourd, Indéplaçable, etc.
