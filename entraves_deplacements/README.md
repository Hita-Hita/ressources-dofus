# Entraves et déplacements

Ce document vise à expliquer le fonctionnement des états d'entrave comme <img src="../pics/etats/pesanteur.svg" width=3.8%> **Pesanteur** ou <img src="../pics/etats/indeplacable.svg" width=3.8%> **Indéplaçable**.

> Il existe des exceptions **uniquement en PvM (ou PvE)** pour certains monstres/bosses. Elles sont abordées dans [la section "Exceptions" dédiée](#exceptions) (ouais ouais putain de Glours qui TP sous pesanteur, je te vois).

## Sommaire

- [Entraves et déplacements](#entraves-et-déplacements)
  - [Sommaire](#sommaire)
  - [I. États d'entrave](#i-états-dentrave)
  - [II. Types de déplacements](#ii-types-de-déplacements)
    - [Détails](#détails)
  - [III. Quel déplacement sous quel état ?](#iii-quel-déplacement-sous-quel-état-)
  - [IV. Sorts de classe](#iv-sorts-de-classe)
    - [1. Quel classe pose quel état ?](#1-quel-classe-pose-quel-état-)
    - [2. Liste exhaustive des sorts d'entrave](#2-liste-exhaustive-des-sorts-dentrave)
      - [Crâ](#crâ)
      - [Ecaflip](#ecaflip)
      - [Eliotrope](#eliotrope)
      - [Eniripsa](#eniripsa)
      - [Enutrof](#enutrof)
      - [Féca](#féca)
      - [Huppermage](#huppermage)
      - [Iop](#iop)
      - [Osamodas](#osamodas)
      - [Ouginak](#ouginak)
      - [Pandawa](#pandawa)
      - [Roublard](#roublard)
      - [Sacrieur](#sacrieur)
      - [Sadida](#sadida)
      - [Sram](#sram)
      - [Steamer](#steamer)
      - [Xélor](#xélor)
      - [Zobal](#zobal)
    - [3. Liste exhaustive des sorts de déplacement](#3-liste-exhaustive-des-sorts-de-déplacement)
      - [Crâ](#crâ-1)
      - [Ecaflip](#ecaflip-1)
      - [Eliotrope](#eliotrope-1)
      - [Eniripsa](#eniripsa-1)
      - [Enutrof](#enutrof-1)
      - [Féca](#féca-1)
      - [Huppermage](#huppermage-1)
      - [Iop](#iop-1)
      - [Osamodas](#osamodas-1)
      - [Ouginak](#ouginak-1)
      - [Pandawa](#pandawa-1)
      - [Roublard](#roublard-1)
      - [Sacrieur](#sacrieur-1)
      - [Sadida](#sadida-1)
      - [Sram](#sram-1)
      - [Steamer](#steamer-1)
      - [Xélor](#xélor-1)
      - [Zobal](#zobal-1)
  - [V. PvM](#v-pvm)
    - [1. Exceptions](#1-exceptions)
    - [2. Monstres notables](#2-monstres-notables)

## I. États d'entrave

Voici une liste exhaustive des états d'entrave, ils sont au nombre de 5 :

- <img src="../pics/etats/indeplacable.svg" width=3.8%> **Indéplaçable**  
- <img src="../pics/etats/enracine.svg" width=3.8%> **Enraciné** (Note : cet état applique aussi **Intaclable** et **Intacleur** : la cible n'est plus taclée et ne peut plus l'être)
- <img src="../pics/etats/pesanteur.svg" width=3.8%> **Pesanteur**
- <img src="../pics/etats/lourd.svg" width=3.8%> **Lourd**
- <img src="../pics/etats/inebranlable.svg" width=3.8%> **Inébranlable**

Chacun de ces états bloque ou ne bloque pas un **type de déplacement**.

## II. Types de déplacements

La liste exhaustive des **types de déplacements** dans Dofus, il y en a 4 :

- <img src="../pics/others/prog.svg" width=3.8%> **Déplacement progressif**, qu'on appellera juste ***Progressif***
  - ce sont les mouvements où l'on voit la cible se déplacer case par case
- <img src="../pics/spells/bond.svg" width=3.8%> **Déplacement instantané**, ou juste ***Instantané***
  - on parle ici d'un mouvement où la cible se déplace instantanément sur la cellule de destination
- <img src="../pics/spells/karcham.svg" width=3.8%> **Portée Pandawa**, ou juste ***Portée***
  - utilisation du sort *Karcham* ou du sort *Varappe* du Pandawa et *hop* tu finis dans les bras du Panda
- <img src="../pics/spells/portail.svg" width=3.8%> **Portail Eliotrope**, ou juste ***Portail***
  - on parle ici du fait de traverser un portail, et de ré-apparaître sur un autre portail
  - le jeu ne considère **PAS** *le Portail Eliotrope* comme un *Déplacement Instantané*

### Détails

Complétons le vocabulaire, avec quelques exemples pour clarifier :

- <img src="../pics/others/prog.svg" width=3.8%> **Déplacement progressif**
  - **Attirance** : la cible est attirée vers le lanceur
    - Attirance du Sacrieur
    - Menace du Iop
    - Thérapie de l'Eliotrope
  - **Poussée** : la cible est poussée par rapport au lanceur
    - Tension du Féca
    - Mot de Frayeur de l'Eniripsa
    - Souffle Alcoolisé du Pandawa
  - **Expulsion** : une entité est poussée par rapport à la cellule ciblée
    - Pulsar du Roublard
    - Flèche de Dispersion du Crâ
    - Convulsion de l'Eliotrope
  - **Succion** : une entité est attirée par rapport à la cellule ciblée
    - Flamme Latérale du Féca
    - Commotion de l'Eliotrope
    - Flèche de Concentration du Crâ
- <img src="../pics/spells/bond.svg" width=3.8%> **Déplacement instantané**
  - **Téléportation** : l'entité est téléportée sur la case ciblée
    - Téléglyphe du Féca
    - Téléportation du Xélor
  - **Transposition** : l'entité échange de place avec une autre
    - Transposition du Sacrieur
    - Vivacité de l'Enutrof
    - Beaucoup de sorts du Xélor en sont capables comme Frappe du Xélor

## III. Quel déplacement sous quel état ?

> Les termes comme ***Progressif*** ou ***Instantané*** pour désigner les différents types de déplacement sont définis dans [la section précédente](#ii-types-de-déplacements).

| | <img src="../pics/etats/indeplacable.svg" width=25%> **Indéplaçable** | <img src="../pics/etats/enracine.svg" width=19%> **Enraciné** | <img src="../pics/etats/pesanteur.svg" width=30%> **Pesanteur** | <img src="../pics/etats/lourd.svg" width=40%> **Lourd** | <img src="../pics/etats/inebranlable.svg" width=25%> **Inébranlable**
| --- | --- | --- | --- | --- | --- |
| <img src="../pics/others/prog.svg" width=25%> ***Progressif*** | ❌ | ❌ | ✔️ | ✔️ | ❌ |
| <img src="../pics/spells/bond.svg" width=25%> ***Instantané*** | ❌ | ❌ ou ✔️ : voir plus bas | ❌ | ✔️ |✔️ |
| <img src="../pics/spells/karcham.svg" width=25%> ***Portée*** | ❌ | ✔️ | ✔️ | ❌ | ✔️ |
| <img src="../pics/spells/portail.svg" width=25%> ***Portail*** | ❌ | ❌ | ✔️ | ✔️ | ✔️ |

---

**Cas particulier du <img src="../pics/spells/bond.svg" width=3.8%> *Déplacement Instantané* sous <img src="../pics/etats/enracine.svg" width=3.8%> *Enraciné***. *~~Ce serait trop facile sinon.~~*

Et ui, ça dépend de qui initie le déplacement ::

- si c'est l'entité *Enracinée* qui essaie d'effectuer un ***Déplacement Instantané*** : ✔️
  - si un Pandawa te stab petit Xélor, tu peux utiliser ta Téléportation !
- si c'est une autre entité qui essaie d'effectuer un ***Déplacement Instantané*** sur une entité *Enracinée* : ❌
  - si un Pandawa stab une autre entité, tu ne pourras pas téléporter cette entité petit Xélor !

## IV. Sorts de classe

### 1. Quel classe pose quel état ?

| | <img src="../pics/etats/indeplacable.svg" width=28%> **Indéplaçable** | <img src="../pics/etats/enracine.svg" width=48%> **Enraciné** | <img src="../pics/etats/pesanteur.svg" width=31%> **Pesanteur** | <img src="../pics/etats/lourd.svg" width=68%> **Lourd** | <img src="../pics/etats/inebranlable.svg" width=33%> **Inébranlable**
| --- | --- | --- | --- | --- | --- |
| <img src="../pics/classes/cra.png" width=15%> **Crâ** | ❌ | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/eca.png" width=15%> **Ecaflip** | ❌ | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/elio.png" width=15%> **Eliotrope** | ❌ | ❌ | ❌ | ❌ | ❌ |
| <img src="../pics/classes/eni.png" width=15%> **Eniripsa** | ❌ | ❌ | ❌ | ❌ | ❌ |
| <img src="../pics/classes/enutrof.png" width=15%> **Enutrof** | ❌ | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/feca.png" width=15%> **Féca** | ❌ | ❌ | ✔️ | ❌ | ✔️ |
| <img src="../pics/classes/hupper.png" width=15%> **Huppermage** | ❌ | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/iop.png" width=15%> **Iop** | ✔️ (que sur le lanceur) | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/osa.png" width=15%> **Osamodas** | ❌ | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/ougi.png" width=15%> **Ouginak** | ❌ | ❌ | ✔️ | ❌ | ✔️ |
| <img src="../pics/classes/panda.png" width=15%> **Pandawa** | ❌ | ✔️ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/roub.png" width=15%> **Roublard** | ✔️ (que sur ses bombes) | ❌ | ✔️ (que sur ses bombes) | ❌ | ❌ |
| <img src="../pics/classes/sacrieur.png" width=15%> **Sacrieur** | ✔️ | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/sadida.png" width=15%> **Sadida** | ❌ | ✔️ | ❌ | ❌ | ❌ |
| <img src="../pics/classes/sram.png" width=15%> **Sram** | ❌ | ❌ | ✔️ | ❌ | ❌ |
| <img src="../pics/classes/steamer.png" width=15%> **Steamer** | ✔️ (que sur ses tourelles) | ❌ | ❌ | ❌ | ❌ |
| <img src="../pics/classes/xelor.png" width=15%> **Xélor** | ❌ | ❌ | ❌ | ❌ | ❌ |
| <img src="../pics/classes/zobal.png" width=15%> **Zobal** | ✔️ (que sur le lanceur) | ❌ | ✔️ | ❌ | ❌ |

### 2. Liste exhaustive des sorts d'entrave

> A venir.

#### Crâ

| Sort | Effet Appliqué | Cible | PO | PO+ | Zone | Contraintes |
| --- | --- | --- | --- | --- | --- | --- |
| Flèche Écrasante | <img src="../pics/etats/pesanteur.svg" width=3.8%> **Pesanteur** | All | 5-7 | ✔️ | Croix diago 1 PO | ❌|

#### Ecaflip

#### Eliotrope

#### Eniripsa

#### Enutrof

#### Féca

#### Huppermage

#### Iop

#### Osamodas

#### Ouginak

#### Pandawa

#### Roublard

#### Sacrieur

#### Sadida

#### Sram

#### Steamer

#### Xélor

#### Zobal

### 3. Liste exhaustive des sorts de déplacement

Liste exhaustive de tous les sorts de classe qui effectuent un déplacement.

> A venir.

#### Crâ

#### Ecaflip

#### Eliotrope

#### Eniripsa

#### Enutrof

#### Féca

#### Huppermage

#### Iop

#### Osamodas

#### Ouginak

#### Pandawa

#### Roublard

#### Sacrieur

#### Sadida

#### Sram

#### Steamer

#### Xélor

#### Zobal

## V. PvM

### 1. Exceptions

**Les exceptions n'existent qu'en PvM.**

> A venir.

### 2. Monstres notables

Quelques monstres notables qui ont ou posent des états d'entrave.

> A venir.
